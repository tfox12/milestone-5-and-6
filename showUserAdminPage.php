<?php
session_start();
require_once 'showTopMenu.php';
require_once 'db_connector.php';

if ( $_SESSION['role'] != 'admin') {
    echo "Please login as an admin<br>";
    exit;
}

$sql_statement = "SELECT * FROM `users_table`";

// The section below will fetch data from the table so that users can find devotions they created

if ($connection) {
    $result= mysqli_query($connection, $sql_statement);
    if ($result) {
        while ($row= mysqli_fetch_assoc($result)) {
            echo "user ID " . $row['id'] . "<br>";
            echo "user name " . $row['user_name'] . "<br>";
            echo "user password " . $row['user_password'] . "<br>";
            echo "user role " . $row['role'] . "<br>";
            
            ?>
            <form action="processDeleteUser.php">
            	<input type="hidden" name="id" value="<?php echo $row['id']; ?>"></input>
            	<button type="submit">Delete</button>
            </form>
            
            <form action="showEditUserForm.php">
            	<input type="hidden" name="id" value="<?php echo $row['id']; ?>"></input>
            	<button type="submit">Edit</button>
            </form>
            <?php
            
            echo "==========<br>";
        }
        
    } else {
        echo "Error with the SQL " . mysqli_error($connection);
    }
    
    
} else {
    echo "Error connecting " . mysqli_connect_error();
}

?>