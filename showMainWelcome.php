<?php 
require_once 'db_connector.php';

$sql_statement = "SELECT * FROM `devotion_table`";

if ($connection) {
    $result= mysqli_query($connection, $sql_statement);
    if ($result) {
        while ($row= mysqli_fetch_assoc($result)) {
?>

<html>
<head>
<link rel="stylesheet" type= "text/css" href="myWelcomeDesign.css">
</head>
<body>

<!-- This page is not finished, but it will be the home page for the blog. -->

<div class="row">
<div class="leftcolumn">
	<div class="card">
		<h1><?php echo $row['devotion_topic'];?></h1> 
		<h5>-<?php echo $row['devotion_title']?>-</h5>
		<div class="fakeimg" style="height:200px;"><?php echo $row['devotion_body'];?></div>
	</div>
	<div class="card">
		<h2>TITLE HEADING</h2>
		<h5>Title description, Sep 2, 2017</h5>
		<div class="fakeimg" style="height:200px;">Image</div>
		<p>Some text..</p>
	</div>
</div>

<div class="rightcolumn">
	<div class="card">
		<h2>About Me</h2>
		<div class="fakeimg" style="height:100px;">Image</div>
		<p>Some text about me..</p>
	</div>
	<div class="card">
		<h3>Popular Post</h3>
		<div class="fakeimg">Image</div><br>
		<div class="fakeimg">Image</div><br>
		<div class="fakeimg">Image</div>
	</div>
	<div class="card">
		<h3>Follow Me</h3>
		<p>Some text..</p>
	</div>
</div>
</div>

<div class="footer"><h2>Footer</h2></div>
</body>
</html>
<?php
       }
        
    } else {
        echo "Error with the SQL " . mysqli_error($connection);
    }
    
    
} else {
    echo "Error connecting " . mysqli_connect_error();
}

?>